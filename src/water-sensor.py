#!/usr/bin/env python3

from flask import Flask, jsonify
from gpiozero import Button
import datetime


app = Flask(__name__)


def get_sensor_state(number):
    with Button(number) as button:
        result = button.is_pressed
    return result


@app.route("/sensor/<int:number>", methods=["GET"])
def sensor(number):
    return jsonify(
        {
            "name": f"WaterSensor#{number}",
            "state": {
                "open": get_sensor_state(number),
                "timestamp": str(datetime.datetime.now()),
            },
        }
    )


if __name__ == "__main__":
    app.run(debug=False, port=9000, host="0.0.0.0")
